# Bilion - Uma linguagem de programação zuada

## Contexto
Eu tenho um grupo de amigos da faculdade o qual a gente
se intitula "bilionários de humanas", em uma época no qual
fazíamos uma matéria de pesquisa operacional, e precisávamos
simular um gerenciamento de uma empresa, com fluxo de caixa e afins.


Bem, a gente não se deu muito bem nessa matéria, mas passamos tranquilamente.
Então, se a gente fosse ficar rico com empreendedorismo, a gente seria
empreendedores de humanas (vender arte na praia).

Basicamente, depois de muito tempo programando em Python, eu decidi
que, se eu for seguir a área como engenheiro de software,
eu precisaria aprender mais de uma linguagem, e com a qual não seria
interpretada e que mexesse com codificação mais baixo nível que não fosse
C/C++. Logo, eu escolhi Rust, pois junta o útil ao agradável.

Então, para aprender Rust, decidi criar uma linguagem de programação
em homenagem aos meus amigos e a minha experiência na faculdade com esses
meus amigos. :)
